package config

object GetData : Config("data.properties") {

    val url: String by lazy {
        properties.getProperty("url")
    }

    val accessKey: String by lazy {
        properties.getProperty("accessKey")
    }
}