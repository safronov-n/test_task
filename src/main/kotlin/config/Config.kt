package config

import java.io.InputStreamReader
import java.util.*

open class Config(private val configFile: String) {

    val properties: Properties by lazy {
        getConfig(configFile)
    }

    @Synchronized
    private fun getConfig(configName: String): Properties {
        val properties = Properties()
        val inputStream = this.javaClass.classLoader.getResourceAsStream(configName)
        val reader = InputStreamReader(inputStream!!, "UTF-8")
        properties.load(reader)
        return properties
    }

}