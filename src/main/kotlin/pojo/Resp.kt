package pojo

data class Resp(
    var ip: String?,
    var type: String?,
    var continent_code: String?,
    var continent_name: String?,
    var country_code: String?,
    var country_name: String?,
    var region_code: String?,
    var region_name: String?,
    var city: String?,
    var zip: String?,
    var latitude: Double?,
    var longitude: Double?,
    var location: Location?
) {

    data class Location(
        var geoname_id: Int?,
        var capital: String?,
        var languages: ArrayList<Language>?,
        var country_flag: String?,
        var country_flag_emoji: String?,
        var country_flag_emoji_unicode: String?,
        var calling_code: String?,
        var is_eu: Boolean,
    )


    data class Language(
        var code: String?,
        var name: String?,
        var native: String?
    )

}
