package enums

enum class Headers(var header: String) {
    ACCESS_KEY("access_key")
}