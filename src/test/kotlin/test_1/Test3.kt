package test_1

import com.codeborne.selenide.WebDriverRunner
import java.lang.Exception

class Test3 {

    /**
     * Provide a code fragment that switches the current browser to another tab (consider there are only 2 tabs).
     * Please comment on the code in detail.
     */

    fun switchTab() {
        // Узнаем количество вкладок
        val countTab = WebDriverRunner.getWebDriver().windowHandles.count()
        // Проверяем что вкладок реально 2 и если это не так, то выкидываем исключение
        if (countTab == 2) {
            //Берем имена всех вкладок и помещаем их в лист
            val tabs: ArrayList<String> = ArrayList(WebDriverRunner.getWebDriver().windowHandles)
            // Переключаемся на последнюю вкладку, для это из списка берем последний элемент (-1) для того чтобы не вышли
            // за рамки и не словили ошибку
            WebDriverRunner.getWebDriver().switchTo().window(tabs[countTab - 1])
        } else throw Exception("Вкладок больше или меньше 2")
    }
}