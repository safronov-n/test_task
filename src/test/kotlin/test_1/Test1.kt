package test_1

import com.codeborne.selenide.Selenide.element
import org.openqa.selenium.By
import org.openqa.selenium.StaleElementReferenceException


class Test1 {

    /**
     * How would you handle a StaleElementReferenceException?
     * Please provide a code fragment or fragments, if you know about several common ways of handling.
     */

    // обработка ошибки в блоке try/catch с повторением поиска элемента
    fun clickElement() {
        for (i in 0..10) {
            try {
                element(By.xpath("//*[@class='...']/..")).click()
                break
            } catch (e: StaleElementReferenceException) {
                println(e)
            }
        }
    }


}