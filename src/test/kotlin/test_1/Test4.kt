package test_1

import com.codeborne.selenide.Condition
import com.codeborne.selenide.Selenide.element
import com.codeborne.selenide.WebDriverRunner
import org.openqa.selenium.By
import java.time.Duration


class Test4 {

    /**
     * Imagine the following scenario: you click on a button that should redirect you to a new page,
     * but the page URL stays the same and the DOM structure stays almost the same.
     * Provide a code fragment to wait explicitly for the page to unload.
     * Waiting for the page to load is not necessary (perhaps you want to gracefully handle some errors in case you are left on the same page).
     */

    fun waitElement() {
        val driver = WebDriverRunner.getWebDriver()

        //явное ожидание загрузки старницы
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(10))

        //явное ожидание появления элемента и последующее нажатие на него
        element(By.xpath("//*[@class='...']")).should(Condition.appear,Duration.ofSeconds(10)).click()

        //явное ожидание исчезновения элемента
        element(By.xpath("//*[contains(text(),'...')]")).should(Condition.hidden,Duration.ofSeconds(10))
    }
}