package test_1

import config.GetData
import enums.Headers
import pojo.Resp
import io.restassured.RestAssured
import io.restassured.http.ContentType
import org.junit.Test

class Test2 {

    /**
     * Go to https://freegeoip.io/ and get a free API key (be aware that it is limited to 100 requests / mo).
     * Call their API with that key, using JSON format, and perform the following actions:
     * a) Assert the response code;
     * b) Parse the response;
     * c) Assert your latitude and longitude with a 0.01° tolerance (assume you know your actual lat and lon).
     */

    @Test
    fun test() {
        val response = RestAssured.given()
            .`when`()
            .queryParam(Headers.ACCESS_KEY.header, GetData.accessKey)
            .contentType(ContentType.JSON)
            .get(GetData.url)
            .then().statusCode(200)
            .extract().`as`(Resp::class.java)
        assert(response.longitude.toString().contains("30.61") && response.latitude.toString().contains("36.85"))
    }

}